﻿namespace TreeView
{
    using System;
    using System.ComponentModel;
    using ViewModels;

    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            (DataContext as IDisposable)?.Dispose();
        }
    }
}
