﻿namespace TreeView.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Reactive.Disposables;
    using System.Reactive.Linq;
    using DynamicData;
    using DynamicData.Binding;
    using Infrastructure;

    public class MainWindowViewModel : AbstractNotifyPropertyChanged, IDisposable
    {
        readonly IDisposable _cleanup;
        readonly DiscographyService _discographyService = new DiscographyService();
        readonly ReadOnlyObservableCollection<ArtistViewModel> _artists;
        string _activeDescription;

        public MainWindowViewModel()
        {
            var artists = _discographyService.Artists.Connect()
                            .Transform(x => new ArtistViewModel(x))
                            .Publish();

            _cleanup = StableCompositeDisposable.Create(
                artists
                    .Bind(out _artists)
                    .DisposeMany()
                    .Subscribe(),
                artists
                    .TransformMany(x => x.Albums, x => x)
                    .WhenPropertyChanged(x => x.IsActive)
                    .Subscribe(x => ActiveDescription = x.Value ? x.Sender.Description : null),
                artists.Connect()
                );

            _discographyService.Refresh();
        }

        public ReadOnlyObservableCollection<ArtistViewModel> Artists => _artists;

        public string ActiveDescription
        {
            get => _activeDescription;
            set => SetAndRaise(ref _activeDescription, value);
        }

        public void Dispose() => _cleanup.Dispose();
    }
}
