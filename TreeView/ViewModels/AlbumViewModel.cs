﻿namespace TreeView.ViewModels
{
    using System;
    using System.Linq;
    using System.Reactive.Disposables;
    using DynamicData.Binding;
    using Models;

    public sealed class AlbumViewModel : AbstractNotifyPropertyChanged, IDisposable
    {
        readonly Album _model;
        bool _isActive;
        readonly IDisposable _cleanup;

        public AlbumViewModel(Album model)
        {
            _model = model;
            Songs = _model.Songs.Select(x => new SongViewModel(x)).ToArray();

            _cleanup = StableCompositeDisposable.Create(Songs.Select(x => x
                .WhenPropertyChanged(xx => xx.IsActive)
                .Subscribe(xx => IsActive = Songs.Any(xxx => xxx.IsActive))
            ));
        }

        public SongViewModel[] Songs { get; }

        public string Title => _model.Title;

        public string Description => _model.Description;

        public bool IsActive
        {
            get => _isActive;
            set => SetAndRaise(ref _isActive, value);
        }

        public void Dispose() => _cleanup.Dispose();
    }
}