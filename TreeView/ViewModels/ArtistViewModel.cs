﻿namespace TreeView.ViewModels
{
    using System;
    using System.Linq;
    using System.Reactive.Disposables;
    using Models;

    public class ArtistViewModel : IDisposable
    {
        readonly Artist _model;
        readonly IDisposable _cleanup;

        public ArtistViewModel(Artist model)
        {
            _model = model;
            Albums = _model.Albums.Select(x => new AlbumViewModel(x)).ToArray();

            _cleanup = StableCompositeDisposable.Create(Albums.OfType<IDisposable>());
        }

        public AlbumViewModel[] Albums { get; }

        public string Name => _model.Name;

        public void Dispose() => _cleanup.Dispose();
    }
}
