﻿namespace TreeView.ViewModels
{
    using DynamicData.Binding;
    using Models;

    public sealed class SongViewModel : AbstractNotifyPropertyChanged
    {
        readonly Song _model;
        bool _isActive;

        public SongViewModel(Song model) => _model = model;

        public string Title => _model.Title;

        public string Length => _model.Length;

        public bool IsActive
        {
            get => _isActive;
            set => SetAndRaise(ref _isActive, value);
        }
    }
}