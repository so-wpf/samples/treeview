﻿namespace TreeView.Models
{
    public sealed class Album
    {
        public string Title { get; set; }
        public Song[] Songs { get; set; }
        public string Description { get; set; }
    }
}