﻿namespace TreeView.Models
{
    public sealed class Song
    {
        public string Title { get; set; }
        public string Length { get; set; }
    }
}