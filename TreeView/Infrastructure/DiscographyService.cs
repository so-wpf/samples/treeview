﻿namespace TreeView.Infrastructure
{
    using System.IO;
    using System.Linq;
    using DynamicData;
    using Models;
    using Newtonsoft.Json;

    public sealed class DiscographyService
    {
        readonly SourceCache<Artist, string> _artists = new SourceCache<Artist, string>(x => x.Name);

        public IObservableCache<Artist, string> Artists => _artists.AsObservableCache();

        //in an actual app we'd be pulling fresh results here and merging it back into our main list/cache
        public void Refresh()
        {
            var sourcePath = Path.Combine(Path.GetDirectoryName(typeof(DiscographyService).Assembly.Location),
                "data.json");

            if (!File.Exists(sourcePath))
            {
                _artists.Clear();
                return;
            }

            using (var stream = new FileStream(sourcePath, FileMode.Open, FileAccess.Read))
            using (var reader = new StreamReader(stream))
            {
                var buffer = reader.ReadToEnd();

                _artists.Edit(x =>
                {
                    var artists = JsonConvert.DeserializeObject<Artist[]>(buffer);

                    x.Remove(x.Keys.Except(artists.Select(xx => xx.Name)));
                    x.AddOrUpdate(artists);
                });
            }
        }
    }
}
