## Treeview Sample

This sample shows how you can use an implicit style to target IsSelected of TreeViewItem. Although the sample uses [DynamicData](https://github.com/RolandPheasant/DynamicData) based manipulations, the technique isn't specific to observables. At the core it's a simple binding like this:

```xml
<Style TargetType="TreeViewItem">
    <Setter Property="IsSelected" Value="{Binding IsActive}" />
</Style>
```

At which point you have access to selected item via the boolean property in viewmodel.

For example if you're in single selection mode:

```csharp
var selectedItem = items.Single(x => x.IsSelected);
```

This same technique can be used on all controls that support selections for example `ListView` via `ListViewItem.IsSelected` or `DataGrid` via `DataGridCell.IsSelected`.


#### Screenshot

![Screenshot](Screenshot.png)

## Credits

The sample makes use of the following excellent libraries:

[DynamicData](https://github.com/RolandPheasant/DynamicData) for change notification management & reaction  
[MahApps.Metro](https://github.com/MahApps/MahApps.Metro) For the base theme  
[MaterialDesignInXamlToolkit](https://github.com/ButchersBoy/MaterialDesignInXamlToolkit) For the material color scheme with `MaterialDesignThemes.MahApps` theme compatability shim applied  
[Newtonsoft Json](https://www.newtonsoft.com/json) for data.json deserialization used as sample data  
[System.Reactive](https://github.com/Reactive-Extensions/Rx.NET) the core reactive library from Microsoft upon which this observable world is built  

[WPF SO Chat Residents](https://chat.stackoverflow.com/rooms/18165/wpf) for their constant mentoring and invaluable advice over the years while patiently teaching me their ways of WPF through gentle nudges :)